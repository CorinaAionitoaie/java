/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package graph;

import edu.uci.ics.jung.algorithms.layout.FRLayout;
import java.io.*;
import java.util.*;
import java.util.Map.Entry;
import java.awt.image.BufferedImage;
import java.awt.Point.*;
import javax.swing.JPanel;
import edu.uci.ics.jung.visualization.VisualizationViewer;
import edu.uci.ics.jung.visualization.*;
import edu.uci.ics.jung.graph.*;
import edu.uci.ics.jung.graph.event.GraphEvent;
//import edu.uci.ics.jung.graph.event.GraphEvent.Vertex;
import edu.uci.ics.jung.graph.impl.SparseVertex;
import edu.uci.ics.jung.graph.impl.UndirectedSparseEdge;
import edu.uci.ics.jung.utils.UserDataContainer;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.geom.*;
import javax.imageio.ImageIO;

/**
 *
 * @author Corina
 */
public class Graph {

    public BufferedImage createGraph(String file) {
        List<String> nodes = new LinkedList<>();
        Map<String, String> muchii = new HashMap<String, String>();
        boolean m = false;
        try (BufferedReader br = new BufferedReader(new FileReader(file))) {
            System.out.println(file);
            String line = null;
            while ((line = br.readLine()) != null) {
                if (line.contains("#")) {
                    m = true;
                    continue;
                }
                System.out.println(line);
                if (m == true) {
                    muchii.put(line.split(" ")[0], line.split(" ")[1]);
                } else {
                    nodes.add(line.split(" ")[0]);
                }
                System.out.println(muchii);
            }
        } catch (IOException ex) {
            System.out.println(ex.getMessage());
        }

        printGraph(nodes, muchii);
        return buildGraphFromCollection(muchii);
    }

    private void printGraph(List<String> nodes, Map<String, String> mucs) {

        for (int i = 0; i < nodes.size(); i++) {
            System.out.println("Node: " + nodes.get(i));
        }
        Set<Entry<String, String>> entries = mucs.entrySet();
        for (Entry<String, String> e : entries) {
            System.out.println(e.getKey() + "  " + e.getValue());
        }
    }

    private BufferedImage buildGraphFromCollection(Map<String, String> graph) {
        edu.uci.ics.jung.graph.UndirectedGraph g = new edu.uci.ics.jung.graph.impl.UndirectedSparseGraph();
        Set<Entry<String, String>> entries = graph.entrySet();
        for (Entry<String, String> entry : entries) {
            Vertex v1 = new SparseVertex();
            v1.addUserDatum("name", entry.getKey(), new UserDataContainer.CopyAction.Clone());
            g.addVertex(v1);
            Vertex v2 = new SparseVertex();
            v2.addUserDatum("name", entry.getValue(), new UserDataContainer.CopyAction.Clone());
            g.addVertex(v2);
            Edge e = new UndirectedSparseEdge(v1, v2);
            g.addEdge(e);
        }
        FRLayout l = new FRLayout(g);
        PluggableRenderer r = new PluggableRenderer();
        VisualizationViewer vv = new VisualizationViewer((Layout) l, r);

        VisualizationImageServer vis =
                new VisualizationImageServer((edu.uci.ics.jung.algorithms.layout.Layout) vv.getGraphLayout(), vv.getGraphLayout().getCurrentSize());

//        BufferedImage image = (BufferedImage) vis.getImage(
//                new Point2D.Double(vv.getGraphLayout().getCurrentSize().getWidth() / 2,
//                vv.getGraphLayout().getCurrentSize().getHeight() / 2),
//                new Dimension(vv.getGraphLayout().getCurrentSize()));
//
//        File outputfile = new File("graph.png");
//
//        try {
//            ImageIO.write(image, "png", outputfile);
//        } catch (IOException e) {
//            // Exception handling
//        }

		vv.setBackground(Color.BLUE);
		JPanel jpanel = new JPanel();
		jpanel.setPreferredSize(new Dimension(500, 500));
		jpanel.setSize(new Dimension(500, 500));
		jpanel.add(vv);
		System.out.println("Jpanel size " + jpanel.getWidth() + "   " + jpanel.getHeight());
		BufferedImage bi = new BufferedImage(jpanel.getWidth(), jpanel.getHeight(), BufferedImage.TYPE_INT_ARGB);
		jpanel.paint(bi.getGraphics());
		return bi;
    }
}
