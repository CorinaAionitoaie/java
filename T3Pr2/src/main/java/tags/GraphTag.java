/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package tags;

import graph.Graph;
import javax.servlet.jsp.*;
import javax.servlet.jsp.tagext.*;
import java.io.*;
import java.util.*;
import javax.imageio.ImageIO;
import servlets.GraphBuilderServlet;

/**
 *
 * @author Corina
 */
public class GraphTag extends TagSupport implements DynamicAttributes {

	private Map<String, Object> attributes = new HashMap<String, Object>();

	@Override
	public int doStartTag() throws JspException {
		Tag parent = findAncestorWithClass(this, DrawTag.class);
		setParent(parent);
		//JspWriter writer = pageContext.getOut();
		String file = (String) attributes.get("file");
		String type = (String) attributes.get("type");
		String size = (String) attributes.get("size");
//		try {
//			writer.append("File: " + file + "\tType: " + type + "\tSize: " + size);
//			writer.flush();
//		} catch (IOException ioe) {
//			System.out.println(ioe.getMessage());
//		}
		if (file != null) {
			System.out.println(pageContext.getSession().getId());
			Graph g = new Graph();
			GraphBuilderServlet.addImage(pageContext.getSession().getId(), g.createGraph(pageContext.getRequest().getRealPath(file)));
			String image = "<img src='GraphBuilderServlet' />";
			
			try {
				pageContext.getResponse().getWriter().print(image);
			} catch (IOException ioe) {
				System.out.println(ioe.getMessage());
			}
		}
		return EVAL_BODY_INCLUDE;
	}

	@Override
	public void setDynamicAttribute(String uri, String name, Object value) throws JspException {
		attributes.put(name, value);
	}
}
