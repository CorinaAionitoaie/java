/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package tags;

import java.io.IOException;
import javax.servlet.jsp.*;
import javax.servlet.jsp.tagext.*;
import java.util.*;

/**
 *
 * @author Corina
 */
public class DrawTag extends TagSupport  {

	private String color;
	private String layout;

	public void setColor(String color) {
		this.color = color;
	}

	public String getColor() {
		return color;
	}

	public void setLayout(String layout) {
		this.layout = layout;
	}

	public String getLayout() {
		return layout;
	}

	@Override
	public int doStartTag() throws JspException {
//		JspWriter writer = pageContext.getOut();
//		try {
//			writer.append("Color: " + color + "\nLayout: " + layout);
//		} catch (IOException ioe) {
//			System.out.println(ioe.getMessage());
//		}
		return EVAL_BODY_INCLUDE;
	}
}
