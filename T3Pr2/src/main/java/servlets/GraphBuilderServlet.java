/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package servlets;

import java.awt.image.BufferedImage;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.*;
import java.io.*;
import javax.imageio.ImageIO;

/**
 *
 * @author caionitoaie
 */
public class GraphBuilderServlet extends HttpServlet {

	private static final Map<String, BufferedImage> images = new HashMap<>();

	public static final void addImage(String id, BufferedImage bi) {
		images.put(id, bi);
	}

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		response.setContentType("image/png");
		BufferedImage bi = images.get(request.getSession().getId());
		
		System.out.println("!!!!! "+ bi.getData().getDataBuffer().getSize());
		ImageIO.write(bi, "png", response.getOutputStream());
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
	}
}
