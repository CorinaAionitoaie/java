<%-- 
    Document   : index
    Created on : Nov 6, 2013, 8:13:50 PM
    Author     : Corina
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="/tlds/tagLibrary.tld" prefix="g"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <h1>Hello World!</h1>
        <g:draw color="red" layout="rotational">
            <g:graph file="resources/files/graf.tgf" type="directional" size="5">
              1 "Unu"
			  2 "Doi"
			  #
			  1 2 "muchie de la 1 la 2"
            </g:graph>
       
        </g:draw>
       
    </body>
</html>
