<%-- 
    Document   : testinjsp
    Created on : Nov 4, 2013, 9:14:34 PM
    Author     : Corina
--%>

<%@page contentType="text/html"%>
<!DOCTYPE html>
<html>
    <head>
        
        <title>Integrals</title>
    </head>
    <body>
        <h1>Integrals</h1>
        
        Integration is an important concept in mathematics and, together with its inverse,
        differentiation, is one of the two main operations in calculus. <br/>
        Given a function f of a real variable x and an interval [a, b] of the real line, the definite integral
        <br/>
        <code>int_a^b f(x)dx </code><br/> 
        <h2>Introduction</h2>

        To start off, consider the curve <code>y = f(x)</code> between x = 0 and <code>x = 1</code> with <code>f(x) = sqrt x </code>. We ask:<br/>     
        What is the area under the function f, in the interval from 0 to 1?
        and call this (yet unknown) area the integral of f. The notation for this integral will be<br/>
        <code>int_0^1 sqrt (x)dx</code>
        <br/>

        As a first approximation, look at the unit square given by the sides<code> x = 0</code> to<code> x = 1</code> and <code>y = f(0) = 0</code> and <code>y = f(1) = 1</code>.
        Its area is exactly 1. As it is, the true value of the integral must be somewhat less.
        Decreasing the width of the approximation rectangles shall give a better result; 
        so cross the interval in five steps, using the approximation points 0, <code>sqrt (1/5)</code>, <code>sqrt (2/5)</code>, and so on to<code> sqrt 1 =1</code> 
        Summing the areas of these rectangles, we get a better approximation for the sought integral, namely
        <br/>
        <code>sqrt (1/5) (1/5 - 0) + sqrt (2/5) ( 2/5 - 1/5 ) + ... + sqrt (5/5) (5/5-4/5) ~~ 0.7497 </code>
    </body>
</html>
