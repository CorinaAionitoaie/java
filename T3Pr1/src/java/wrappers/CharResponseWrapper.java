/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package wrappers;
import javax.servlet.http.HttpServletResponseWrapper;
import java.io.*;
import javax.servlet.http.HttpServletResponse;
/**
 *
 * @author Corina
 */
public class CharResponseWrapper extends HttpServletResponseWrapper {
   private CharArrayWriter output;
   @Override
   public String toString() {
      return output.toString();
   }
   public CharResponseWrapper(HttpServletResponse response){
      super(response);
      output = new CharArrayWriter();
   }
   @Override
   public PrintWriter getWriter(){
      return new PrintWriter(output);
   }
}
  
