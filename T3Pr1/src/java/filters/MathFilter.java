/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package filters;

import java.io.IOException;
import java.io.PrintStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.*;
import java.io.*;


import wrappers.CharResponseWrapper;

/**
 *
 * @author Corina
 */
public class MathFilter implements Filter {

    private static final boolean debug = true;
    // The filter configuration object we are associated with.  If
    // this value is null, this filter instance is not currently
    // configured. 
    private FilterConfig filterConfig = null;

    public MathFilter() {
    }

    private void doBeforeProcessing(ServletRequest request, ServletResponse response)
            throws IOException, ServletException {
        if (debug) {
            log("MathFilter:DoBeforeProcessing");
        }

        String userAgent = getUserAgent((HttpServletRequest)request);
        Browser browser = getBrowser(userAgent);
        if(browser.equals(Browser.CHROME) || browser.equals(Browser.UNKNOWN)){
            response.getWriter().append("Pentru a vizualiza pagina la o calitate mai buna folositi Mozilla Firefox sau IE+MathPlayer");
        }
        if(browser.equals(Browser.IE) && !isMathPlayerInstalled(userAgent)){
            response.getWriter().append("Pentru a vizualiza pagina la o calitate mai buna instalati MathPlayer sau deschideti pagina folosind Mozilla Firefox");
        }
       
        
    }

    private void doAfterProcessing(ServletRequest request, ServletResponse response)
            throws IOException, ServletException {
        if (debug) {
            log("MathFilter:DoAfterProcessing");
        }

        
    }

    
    @Override
    public void doFilter(ServletRequest request, ServletResponse response,
            FilterChain chain)
            throws IOException, ServletException {

        if (debug) {
            log("MathFilter:doFilter()");
        }

        doBeforeProcessing(request, response);

        PrintWriter out = response.getWriter();
        CharResponseWrapper wrapper = new CharResponseWrapper((HttpServletResponse) response);
        chain.doFilter(request, wrapper);
        String contentType = wrapper.getContentType();
        System.out.println(getUserAgent((HttpServletRequest)request));
        if (wrapper.getContentType().contains("text/html")) {
            CharArrayWriter caw = new CharArrayWriter();
            String wrapperString = wrapper.toString();
            wrapperString = wrapperString.replace("</head>",
                    "<script type='text/javascript' src='../resources/ASCIIMathML.js'></script></head>");
            wrapperString = wrapperString.replace("<code>", "amath ");
            wrapperString = wrapperString.replace("</code>", " endamath");
            caw.write(wrapperString);
           // System.out.println(wrapperString);
            response.setContentLength(caw.toString().length());
            out.write(caw.toString());
        } else {
            out.write(wrapper.toString());
        }
        out.close();
        doAfterProcessing(request, response);
    }

    /**
     * Return the filter configuration object for this filter.
     */
    public FilterConfig getFilterConfig() {
        return (this.filterConfig);
    }

    /**
     * Set the filter configuration object for this filter.
     *
     * @param filterConfig The filter configuration object
     */
    public void setFilterConfig(FilterConfig filterConfig) {
        this.filterConfig = filterConfig;
    }

    /**
     * Destroy method for this filter
     */
    @Override
    public void destroy() {
    }

    /**
     * Init method for this filter
     */
    @Override
    public void init(FilterConfig filterConfig) {
        this.filterConfig = filterConfig;
        if (filterConfig != null) {
            if (debug) {
                log("MathFilter:Initializing filter");
            }
        }
    }

    /**
     * Return a String representation of this object.
     */
    @Override
    public String toString() {
        if (filterConfig == null) {
            return ("MathFilter()");
        }
        StringBuilder sb = new StringBuilder("MathFilter(");
        sb.append(filterConfig);
        sb.append(")");
        return (sb.toString());
    }

    private void sendProcessingError(Throwable t, ServletResponse response) {
        String stackTrace = getStackTrace(t);

        if (stackTrace != null && !stackTrace.equals("")) {
            try {
                response.setContentType("text/html");
                try (PrintStream ps = new PrintStream(response.getOutputStream())) {
                    PrintWriter pw = new PrintWriter(ps);
                    pw.print("<html>\n<head>\n<title>Error</title>\n</head>\n<body>\n"); //NOI18N

                    // PENDING! Localize this for next official release
                    pw.print("<h1>The resource did not process correctly</h1>\n<pre>\n");
                    pw.print(stackTrace);
                    pw.print("</pre></body>\n</html>"); //NOI18N
                    pw.close();
                }
                response.getOutputStream().close();
            } catch (Exception ex) {
            }
        } else {
            try {
                try (PrintStream ps = new PrintStream(response.getOutputStream())) {
                    t.printStackTrace(ps);
                }
                response.getOutputStream().close();
            } catch (Exception ex) {
            }
        }
    }

    public static String getStackTrace(Throwable t) {
        String stackTrace = null;
        try {
            StringWriter sw = new StringWriter();
            PrintWriter pw = new PrintWriter(sw);
            t.printStackTrace(pw);
            pw.close();
            sw.close();
            stackTrace = sw.getBuffer().toString();
        } catch (Exception ex) {
        }
        return stackTrace;
    }

    public void log(String msg) {
        filterConfig.getServletContext().log(msg);
    }
    private static String getUserAgent(HttpServletRequest request){
        String userAgent = request.getHeader("user-agent");
        return userAgent;
    }
    private Browser getBrowser(String userAgent){
        if(userAgent.toLowerCase().contains("firefox"))
            return Browser.FIREFOX;
        if(userAgent.toLowerCase().contains("chrome"))
            return Browser.CHROME;
        if(userAgent.toLowerCase().contains("trident"))
            return Browser.IE;
        return Browser.UNKNOWN;
    }
    private boolean isMathPlayerInstalled(String userAgent){
        if(userAgent.toLowerCase().contains("mathplayer"))
            return true;
        return false;
    }
}
enum Browser{
    FIREFOX, CHROME,IE,UNKNOWN;
}