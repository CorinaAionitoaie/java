/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package servlet;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.util.Map;
import java.util.Set;
import javax.servlet.ServletContext;
import static constants.Constants.*;

/**
 *
 * @author Corina
 */
public class Controller extends HttpServlet {

    private boolean addPagesToHref = true;

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        ServletContext context = getServletContext();
        String path = getCurrentPage(request.getRequestURL().toString().trim());
        PrintWriter out = response.getWriter();
        response.setContentType("text/html");
        try {
            out.print(buildResponse(context, path));
            out.flush();
        } catch (IOException ioe) {
            System.out.println(ioe.getMessage());
        } finally {
            out.close();
        }
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

    private String getCurrentPage(String requestString) {

        String[] strings = requestString.split("/");

        addPagesToHref = true;
        if (strings != null) {
            //System.out.println("ends with " + strings[strings.length - 1]);
            if (strings[strings.length - 1].endsWith("html")) {
                addPagesToHref = false;
                return strings[strings.length - 1];
            } else {
                addPagesToHref = true;
                return "main.html";

            }
        }
        return null;
    }

    private String buildResponse(ServletContext context, String currentPage) throws IOException {

        String rulesFilePath = context.getRealPath(RULES_FILE);
        String currentPageRealPath = context.getRealPath("/pages/" + currentPage);

        Map<String, String> links = NavigationRules.getLinksForPage(currentPage, new File(rulesFilePath));
        BufferedReader br = new BufferedReader(new FileReader(currentPageRealPath));
        StringBuffer response = new StringBuffer();
        String line = null;

        while ((line = br.readLine()) != null) {
            if (line.contains("</body>")) {
                if (links != null) {
                    response.append("<ul>");
                    Set<String> keys = links.keySet();
                    for (String s : keys) {
                        response.append("<li><a href=\"");
                        if (addPagesToHref == true) {
                            response.append("pages/");
                        }
                        String str = s.toLowerCase().substring(s.indexOf("_")+1);
                        response.append(links.get(s)).append("\">").append(str).append("</a></li>");
                    }
                    response.append("</ul>");
                }
                response.append(NavigationRules.getAllRules(new File(rulesFilePath)));
            }
            
            response.append(line);
        }
        br.close();
        return response.toString();
    }
}
