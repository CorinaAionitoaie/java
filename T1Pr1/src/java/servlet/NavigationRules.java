/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package servlet;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.*;
import java.io.*;

/**
 *
 * @author Corina
 */
public class NavigationRules {

    public static Map<String, String> getLinksForPage(String currentPage, File rulesFile) {
        Map<String, String> links = new HashMap<>();
        try {
            try (BufferedReader br = new BufferedReader(new FileReader(rulesFile))) {
                String page = null;
                while ((page = br.readLine()) != null) {
                    String[] rule = page.split(" ");
                    if (rule != null && rule.length >= 3 && rule[0].trim().equals(currentPage)) {
                        links.put(rule[1], rule[2]);
                    }
                }
            }
        } catch (FileNotFoundException fne) {
            System.out.println(fne.getMessage());
        } catch (IOException ioe) {
        }
        return links;
    }

    public static String getAllRules(File rulesFile) {
        StringBuilder rules = new StringBuilder();
        try {
            try (BufferedReader br = new BufferedReader(new FileReader(rulesFile))) {
                String line = null;
                while ((line = br.readLine()) != null) {
                    rules.append(line);
                    rules.append("<br/>");
                }
            }
        } catch (FileNotFoundException fne) {
        } catch (IOException ioe) {
        }

        return "<p>" + rules.toString().replaceAll(".html", " page ").replaceAll("\\sCLICK_[A-Z]+\\s", " -> ") + "</p>";
    }
}
