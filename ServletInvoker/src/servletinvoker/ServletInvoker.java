/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package servletinvoker;

import java.net.*;
import java.io.*;

/**
 *
 * @author Corina
 */
public class ServletInvoker extends Thread {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws Exception {
        for (int i = 0; i < 10; i++) {
            new ServletInvoker().start();
        }
    }

    @Override
    public void run() {
        synchronized (ServletInvoker.class) {
            try {
                String address = "http://localhost:8080/T1Pr2";
                URL url = new URL(address);
                URLConnection connection = url.openConnection();
                InputStream stream = (InputStream) connection.getContent();
                BufferedReader br = new BufferedReader(new InputStreamReader(stream));
                String line = null;
                System.out.println("\n" + Thread.currentThread().getName());
                while ((line = br.readLine()) != null) {
                    System.out.println(line);
                }
            } catch (Exception e) {
            }
        }
    }
}
