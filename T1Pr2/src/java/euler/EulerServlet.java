/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package euler;

import java.io.IOException;
import java.io.PrintWriter;
import java.math.BigDecimal;
import java.math.RoundingMode;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.annotation.WebInitParam;
import java.io.*;
import static constants.Constants.*;
import javax.servlet.ServletContext;
import java.net.URL;
import javax.servlet.ServletConfig;

/**
 *
 * @author Corina
 */
@WebServlet(name = "EulerServlet",
        urlPatterns = {"/EulerServlet", "Euler"})
public class EulerServlet extends HttpServlet {

    String filePath;

    /**
     * Processes requests for both HTTP
     * <code>GET</code> and
     * <code>POST</code> methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        ServletContext context = getServletContext();
        filePath = context.getRealPath(EULER_NUMBER_FILE);
        response.setContentType("text/html");
        try (PrintWriter out = response.getWriter()) {
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Euler Servlet</title>");
            out.println("<link rel=\"stylesheet\" href=\"resources/css/style.css\">");
            out.println("</head>");
            out.println("<body>");
            out.println("<div id=\"form-div\">");

            try {
                int fractionDigits = Integer.parseInt(request.getParameter("zecimale").trim());
                Euler euler = new Euler(fractionDigits);
                BigDecimal eulersNumber = euler.getEulersNumber();
                euler.writeNumberToFile(new File(filePath));
                out.println("<p>Numarul lui Euler ( " + fractionDigits + " zecimale ) = " + eulersNumber + "</p>");
            } catch (NumberFormatException nfe) {
                out.println("<p>Numar de zecimale incorect (trebuie sa fie un numar intreg mai mare decat 0)</p>");
            } catch (IllegalArgumentException iae) {
                out.println("<p>Numar de zecimale incorect (trebuie sa fie un numar mai mare decat 0)</p>");
            }
            out.println("</div>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP
     * <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP
     * <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
