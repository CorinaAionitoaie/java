/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package euler;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.math.BigDecimal;
import java.math.RoundingMode;

/**
 *
 * @author Corina
 */
public class Euler {

    int fractionDigits;
    BigDecimal e;

    public Euler(int fractionDigits) {
        this.fractionDigits = fractionDigits;
        this.e = computeEulersNumber(fractionDigits);
    }

    public int getFractionDigits() {
        return fractionDigits;
    }

    public BigDecimal getEulersNumber() {
        return e;
    }

    public String writeNumberToFile(File file)  {

        BigDecimal numberFromFile = readNumberFromFile(file);
        if (numberFromFile == null && fractionDigits == 0) {
            writeNumberToFile("2", file);
            return "2";
        }
        if (fractionDigits == 0) {
            return "2";
        }
        if (numberFromFile != null && countFractionDigits(numberFromFile) == fractionDigits) {
            return numberFromFile.toString();
        }
        if (numberFromFile == null || (countFractionDigits(numberFromFile) < fractionDigits)) {
            writeNumberToFile(e.toString(), file);
        }
        return e.toString();
    }

    private BigDecimal computeEulersNumber(int fractionDigits) throws IllegalArgumentException {

        if (fractionDigits < 0) {
            throw new IllegalArgumentException();
        }

        BigDecimal result = new BigDecimal(2d);
        if (fractionDigits == 0) {
            return result;
        }

        for (int i = 2; i < 25; i++) {
            BigDecimal bd1 = new BigDecimal(1d);
            bd1 = bd1.divide(BigDecimal.valueOf((double) computeFactorial(i)), fractionDigits, RoundingMode.FLOOR);
            result = result.add(bd1);
        }
        return result;
    }

    private int computeFactorial(int number) {
        int result = 1;
        for (int i = 2; i <= number; i++) {
            result *= i;
        }
       return result;
    }

    private BigDecimal readNumberFromFile(File file) {
        String number = null;
        BufferedReader br = null;
        try {
            br = new BufferedReader(new FileReader(file));
            number = br.readLine();
            System.out.println("NUmber from file   ->   " + number);
            br.close();
            if (number == null) {
                return null;
            }
        } catch (IOException ioe) {
            ioe.printStackTrace();
        }

        try {
            System.out.println("Number  " + new BigDecimal(number));
            return new BigDecimal(number);
        } catch (NumberFormatException nfe) {
            System.out.println("exxxxxxxxxxxxxxxxx");
            return null;
        }
    }

    private int countFractionDigits(BigDecimal number) {
        int digits;
        if (number.doubleValue() == 2d) {
            return 0;
        }
        try {
            digits = number.toString().trim().split("\\.")[1].length();
            System.out.println(number + "   digits  " + number.toString().trim().split("\\.")[1] + "         " + digits);
        } catch (ArrayIndexOutOfBoundsException aie) {
            digits = 0;
        }
        return digits;
    }

    private void writeNumberToFile(String number, File file) {
        try {
            try (BufferedWriter bw = new BufferedWriter(new FileWriter(file))) {
                bw.write(number);
                bw.flush();
                bw.close();
            }
        } catch (IOException ie) {
            ie.printStackTrace();
        }
    }
}
