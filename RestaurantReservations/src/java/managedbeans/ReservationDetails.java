/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package managedbeans;

import java.util.Date;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.*;
import java.io.Serializable;

/**
 *
 * @author Corina
 */
@ManagedBean(name = "reservationDetails", eager = true)
@SessionScoped
public class ReservationDetails implements Serializable {

    private Integer typeOfTable;
    private String startingHour;
    private Double reservationPeriod;
    private Date reservationDate;

    public String getStartingHour() {
        return startingHour;
    }

    public void setStartingHour(String startingHour) {
        this.startingHour = startingHour;
    }

    public Double getReservationPeriod() {
        return reservationPeriod;
    }

    public void setReservationPeriod(Double reservationPeriod) {
        this.reservationPeriod = reservationPeriod;
    }

    public Date getReservationDate() {
        return reservationDate;
    }

    public void setReservationDate(Date reservationDate) {
        this.reservationDate = reservationDate;
    }

   
    public Integer getTypeOfTable() {
        return typeOfTable;
    }

    public void setTypeOfTable(Integer typeOfTable) {
        this.typeOfTable = typeOfTable;
    }

    @Override
    public String toString() {
        return "\nDate: " + reservationDate + "\nStarting-hour: " + startingHour + "\nPeriod: "
                + reservationPeriod + "\nType of table: " + typeOfTable + "\n";
    }

    public void cleanReservationDetails() {
        this.reservationDate = null;
        this.reservationPeriod = null;
        this.startingHour = null;
        this.typeOfTable = null;
    }
}
