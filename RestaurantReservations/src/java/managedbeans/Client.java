/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package managedbeans;

import javax.faces.bean.ManagedBean;
import java.io.Serializable;
import javax.faces.bean.SessionScoped;

/**
 *
 * @author Corina
 */
@ManagedBean(name="client")
@SessionScoped
public class Client implements Serializable{
 
    private Long id;
    private String name;
    private Long phone;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getPhone() {
        return phone;
    }

    public void setPhone(Long phone) {
        this.phone = phone;
    }
    
    public String toString(){
        return "\nName: "+name+"\nPhone: "+phone+"\n";
    }
}
