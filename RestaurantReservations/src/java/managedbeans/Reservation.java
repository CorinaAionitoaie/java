/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package managedbeans;

import dao.ReservationDAO;
import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;

/**
 *
 * @author Corina
 */
@ManagedBean(name = "reservation")
@SessionScoped
public class Reservation implements Serializable {

    @ManagedProperty("#{client}")
    private Client client;
    @ManagedProperty("#{reservationDetails}")
    private ReservationDetails reservationDetails;
    private List<ReservationDAO> reservationsList = new LinkedList<>();
    private String reservationTemps = "";

    public Client getClient() {
        return client;
    }

    public void setClient(Client client) {
        this.client = client;
    }

    public ReservationDetails getReservationDetails() {
        return reservationDetails;
    }

    public void setReservationDetails(ReservationDetails reservationDetails) {
        this.reservationDetails = reservationDetails;
    }

    public List<ReservationDAO> getReservationsList() {
        return reservationsList;
    }

    public void setReservationsList(List<ReservationDAO> reservationsList) {
        this.reservationsList = reservationsList;
    }

    public String getReservationTemps() {
        return reservationTemps;
    }

    public void setReservationTemps(String reservationTemps) {
        this.reservationTemps = reservationTemps;
    }

    @Override
    public String toString() {
        return client.toString() + reservationDetails.toString();
    }

    public String reserve() {
        for(ReservationDAO r: reservationsList){
            r.persist();
        }
        reservationsList = new LinkedList<>();
        return "index";
    }

    public String addReservationDetails() {
        if (reservationsList.isEmpty()) {
            reservationTemps = "Reservations";
        }
        reservationsList.add(new ReservationDAO(client, reservationDetails));
        reservationDetails.cleanReservationDetails();
        return "reservationPage";
    }
}
