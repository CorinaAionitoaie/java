/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package factoryImpl;

import factoryInterface.DaoFactoryInterface;

/**
 *
 * @author Corina
 */
public abstract class DaoFactory implements DaoFactoryInterface {

     public static DaoFactoryInterface getDaoFactory(int type) {
        switch (type) {
            case RELATIONAL:
                return RdbDaoFactory.getRdbDaoFactoryInstance();
            case OO:
                return OdbDaoFactory.getOdbDaoFactoryInstance();
        }
        return null;
    }
}
