/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package factoryImpl;

import dao.ClientDAO;
import dao.ReservationDetailsDAO;
import factoryInterface.RdbDaoFactoryInterface;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import managedbeans.Client;
import managedbeans.ReservationDetails;

/**
 *
 * @author Corina
 */
public class RdbDaoFactory implements RdbDaoFactoryInterface{

    private static final RdbDaoFactory rdbDaoFactory = new RdbDaoFactory();
    private RdbDaoFactory(){}
    public static RdbDaoFactory getRdbDaoFactoryInstance(){
        return rdbDaoFactory;
    }
    @Override
    public EntityManagerFactory getEntityManagerFactory() {
         return Persistence.createEntityManagerFactory("RestaurantReservationsPostgresPU");
    }
    public static ClientDAO getClientDAO(Client client){
        return new ClientDAO(client);
    }
    public static ClientDAO getClientDAO(){
        return new ClientDAO();
    }
    public static ReservationDetailsDAO getReservationDetailsDAO(ReservationDetails reservationDetails){
        return new ReservationDetailsDAO(reservationDetails);
    }
    public static ReservationDetailsDAO getReservationDetailsDAO(){
        return new ReservationDetailsDAO();
    }
}
