/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package factoryImpl;

import factoryInterface.OdbDaoFactoryInterface;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

/**
 *
 * @author Corina
 */
public class OdbDaoFactory implements OdbDaoFactoryInterface {

    private static final OdbDaoFactory odbDaoFactory = new OdbDaoFactory();

    private OdbDaoFactory() {
    }

    public static OdbDaoFactory getOdbDaoFactoryInstance() {
        return odbDaoFactory;
    }

    @Override
    public EntityManagerFactory getEntityManagerFactory() {
        return Persistence.createEntityManagerFactory("$objectdb/db/restaurant-employee.odb");
    }

}
