/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package factoryInterface;

import javax.persistence.EntityManagerFactory;

/**
 *
 * @author Corina
 */
public interface DaoFactoryInterface {

    public int RELATIONAL = 1;
    public int OO = 2;
    EntityManagerFactory getEntityManagerFactory();
}
