/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import factoryImpl.DaoFactory;
import factoryImpl.OdbDaoFactory;
import factoryImpl.RdbDaoFactory;
import factoryInterface.DaoFactoryInterface;
import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.PersistenceContext;
import javax.persistence.Transient;

/**
 *
 * @author Corina
 */
public abstract class GenericDAO<T> implements Serializable {

    EntityManagerFactory emf;
    EntityManager em;

    public GenericDAO() {
    }

    public void setEmf(EntityManagerFactory emf) {
        this.emf = emf;
    }

    public EntityManagerFactory getEmf() {
        return emf;
    }

    public void setEm(EntityManager em) {
        this.em = em;
    }

    public EntityManager getEm() {
        return em;
    }

    public void initEntityManager(int entityType) {
        switch (entityType) {
            case DaoFactory.OO:
                emf = OdbDaoFactory.getOdbDaoFactoryInstance().getEntityManagerFactory();
                break;
            case DaoFactory.RELATIONAL:
                emf = RdbDaoFactory.getRdbDaoFactoryInstance().getEntityManagerFactory();
                break;
            default:
                return;
        }
        em = emf.createEntityManager();
    }

    public void closeEntityManager() {
        em.close();
        emf.close();
    }

    public void persist(int entityType) {
        initEntityManager(entityType);
        em.getTransaction().begin();
        em.persist(this);
        em.getTransaction().commit();
        closeEntityManager();
    }
}
