/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import factoryImpl.DaoFactory;
import factoryImpl.RdbDaoFactory;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.transaction.Transactional;
import managedbeans.ReservationDetails;

/**
 *
 * @author Corina
 */
@Entity
public class ReservationDetailsDAO extends GenericDAO<ReservationDetailsDAO> {

    @Id
    @Column(name = "RD_ID")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    @Column(name = "RD_TYPE_TABLE")
    private Integer typeOfTable;
    @Column(name = "RD_ST_HOUR")
    private String startingHour;
    @Column(name = "RD_PERIOD")
    private Double reservationPeriod;
    @Column(name = "RD_DATE")
    @Temporal(value = TemporalType.DATE)
    private Date reservationDate;

    public ReservationDetailsDAO() {
    }

    public ReservationDetailsDAO(ReservationDetails reservationDetails) {
        this.reservationDate = reservationDetails.getReservationDate();
        this.reservationPeriod = reservationDetails.getReservationPeriod();
        this.startingHour = reservationDetails.getStartingHour();
        this.typeOfTable = reservationDetails.getTypeOfTable();
    }

    public String getStartingHour() {
        return startingHour;
    }

    public void setStartingHour(String startingHour) {
        this.startingHour = startingHour;
    }

    public Double getReservationPeriod() {
        return reservationPeriod;
    }

    public void setReservationPeriod(Double reservationPeriod) {
        this.reservationPeriod = reservationPeriod;
    }

    public Date getReservationDate() {
        return reservationDate;
    }

    public void setReservationDate(Date reservationDate) {
        this.reservationDate = reservationDate;
    }

    public Integer getTypeOfTable() {
        return typeOfTable;
    }

    public void setTypeOfTable(Integer typeOfTable) {
        this.typeOfTable = typeOfTable;
    }

    @Override
    public String toString() {
        return "\nDate: " + reservationDate + "\nStarting-hour: " + startingHour + "\nPeriod: "
                + reservationPeriod + "\nType of table: " + typeOfTable + "\n";
    }
//    @javax.jdo.annotations.Transactional
//    public void persist() {
////        emf = RdbDaoFactory.getRdbDaoFactoryInstance().getEntityManagerFactory();
////        em = emf.createEntityManager();
//        initEntityManager(DaoFactory.RELATIONAL);
//      //  em.getTransaction().begin();
//        em.persist(this);
//       // em.getTransaction().commit();
//        closeEntityManager();
//    }
}
