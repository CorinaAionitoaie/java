/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import factoryImpl.DaoFactory;
import factoryImpl.OdbDaoFactory;
import factoryImpl.RdbDaoFactory;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.transaction.Transactional;
import managedbeans.Client;

/**
 *
 * @author Corina
 */
@Entity
public class ClientDAO extends GenericDAO<ClientDAO> {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "C_ID")
    private Long id;
    @Column(name = "C_NAME")
    private String name;
    @Column(name = "C_PHONE")
    private Long phone;

    public ClientDAO() {
    }

    public ClientDAO(Client client) {
        this.name = client.getName();
        this.phone = client.getPhone();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getPhone() {
        return phone;
    }

    public void setPhone(Long phone) {
        this.phone = phone;
    }

//    @javax.jdo.annotations.Transactional
//    public void persist() {
////        emf = RdbDaoFactory.getRdbDaoFactoryInstance().getEntityManagerFactory();
////        em = emf.createEntityManager();
//        initEntityManager(DaoFactory.RELATIONAL);
////        em.getTransaction().begin();
//        em.persist(this);
////        em.getTransaction().commit();
//       closeEntityManager();
//    }
}
