/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import factoryImpl.DaoFactory;
import factoryImpl.OdbDaoFactory;
import factoryImpl.RdbDaoFactory;
import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityManager;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.PersistenceContext;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.transaction.Transactional;

/**
 *
 * @author Corina
 */
@Entity
@Table(name = "Employee")
public class EmployeeDAO extends GenericDAO<EmployeeDAO> {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "EMP_ID")
    private Long id;

    @Column(name = "EMP_USERNAME")
    private String username;
    @Column(name = "EMP_PASSWD")
    private String passwd;

    public EmployeeDAO() {
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPasswd() {
        return passwd;
    }

    public void setPasswd(String passwd) {
        this.passwd = passwd;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return "Employee[ id = " + id + "\tusername = " + username + " ]";
    }

    public void persist() {
        persist(DaoFactory.OO);
    }
}
