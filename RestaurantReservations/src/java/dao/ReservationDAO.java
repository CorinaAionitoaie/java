/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import factoryImpl.DaoFactory;
import factoryImpl.RdbDaoFactory;
import java.io.Serializable;
import java.util.*;
import javax.jdo.annotations.Transactional;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Persistence;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceUnit;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import managedbeans.*;

/**
 *
 * @author Corina
 */
@Entity
@Table(name = "Reservation")
public class ReservationDAO extends GenericDAO<ReservationDAO> {

    @Column(name = "ID")
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Id
    private Long id;

    @OneToOne
    ClientDAO client;

    @OneToOne
    ReservationDetailsDAO reservationDetails;

    public ReservationDAO() {
    }

    public ReservationDAO(Client client, ReservationDetails reservationDetails) {
        this.client = RdbDaoFactory.getClientDAO(client);
        this.reservationDetails = RdbDaoFactory.getReservationDetailsDAO(reservationDetails);
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public ClientDAO getClient() {
        return client;
    }

    public void setClient(ClientDAO client) {
        this.client = client;
    }

    public ReservationDetailsDAO getReservationDetails() {
        return reservationDetails;
    }

    public void setReservationDetails(ReservationDetailsDAO reservationDetails) {
        this.reservationDetails = reservationDetails;
    }
 
    public void persist() {
        client.persist(DaoFactory.RELATIONAL);
        reservationDetails.persist(DaoFactory.RELATIONAL);
        this.persist(DaoFactory.RELATIONAL);
    }
}
