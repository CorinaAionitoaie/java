/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package constants;

/**
 *
 * @author Corina
 */
public class Constants {

    public static final String DB_PATH = "C:\\Users\\Corina\\Documents\\NetBeansProjects\\T2\\web\\WEB-INF\\resources\\files\\accounts.txt";
    public static final String COOKIE_PATH = "C:\\Users\\Corina\\Documents\\NetBeansProjects\\T2\\web\\WEB-INF\\resources\\files\\cookie.txt";
    public static final String COOKIE_NAME = "RememberCookie";
    public static final String LOGIN_ERROR = "Login failed";
    public static final String CAPTCHA_ERROR = "Captcha Code is incorrect";
    public static final String OPERATION_ERROR = "Error in operation";
}
