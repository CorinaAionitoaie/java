/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package DummyDatabase;

import java.util.*;
import static constants.Constants.*;
import java.io.*;

/**
 *
 * @author Corina
 */
public class CookiesDatabase {

    public static void storeAssociation(String cookieToken, Integer userAccountId) {
        try (BufferedWriter bw = new BufferedWriter(new FileWriter(COOKIE_PATH))) {
            bw.write(cookieToken + " " + userAccountId);
        } catch (IOException ioe) {
            System.out.println(ioe.getMessage());
        }
    }

    public static Integer retrieveUserId(String cookieToken) {
        try (BufferedReader br = new BufferedReader(new FileReader(COOKIE_PATH))) {
            String line = br.readLine();
            if (line != null) {
                String[] strs = line.split(" ");
                if (strs[0].trim().equals(cookieToken)) {
                    return Integer.parseInt(line.split(" ")[1].trim());
                }
            }
        } catch (FileNotFoundException fne) {
            System.out.println(fne.getMessage());
        } catch (IOException ioe) {
            System.out.println(ioe.getMessage());
        }
        return null;
    }
}
