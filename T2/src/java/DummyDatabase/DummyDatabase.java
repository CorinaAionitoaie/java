/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package DummyDatabase;

import beans.UserAccount;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.LinkedHashMap;
import java.util.Set;
import static constants.Constants.*;
import java.util.Map;

/**
 *
 * @author Corina
 */
public class DummyDatabase {

    public Map<Integer, UserAccount> db;

    public DummyDatabase() throws Exception {
        db = new LinkedHashMap<>();
        db = populateDB();
    }

    private LinkedHashMap<Integer, UserAccount> populateDB() throws Exception {
        ObjectInputStream os = new ObjectInputStream(new FileInputStream(DB_PATH));
        return (LinkedHashMap<Integer, UserAccount>) os.readObject();
    }

    public void addAccount(Integer id, UserAccount ua) {
        db.put(id, ua);
    }

    public void save() throws IOException {
        ObjectOutputStream os = new ObjectOutputStream(new FileOutputStream(DB_PATH));
        os.writeObject(db);
        os.close();
    }

    public boolean checkAccount(String name, String password) throws Exception {
        Map.Entry<Integer, UserAccount> account = retrieveAccount(name);
        if (account != null) {
            if (account.getValue().getPassword().equals(password)) {
                return true;
            }
        }
        return false;
    }

    public Map.Entry<Integer, UserAccount> retrieveAccount(String name) throws Exception {
        Set<Map.Entry<Integer, UserAccount>> entrySet;
        try (ObjectInputStream is = new ObjectInputStream(new FileInputStream(DB_PATH))) {
            Map<Integer, UserAccount> db = (LinkedHashMap<Integer, UserAccount>) is.readObject();
            entrySet = db.entrySet();
        }
        for (Map.Entry<Integer, UserAccount> account : entrySet) {
            if (account.getValue().getName().equals(name)) {
                return account;
            }
        }
        return null;
    }
    public UserAccount retrieveAccountById(Integer id) throws Exception{
        Set<Map.Entry<Integer,UserAccount>> entrySet;
        try (ObjectInputStream is = new ObjectInputStream(new FileInputStream(DB_PATH))) {
            Map<Integer,UserAccount> db =  (LinkedHashMap<Integer, UserAccount>) is.readObject();
            entrySet = db.entrySet();
        }
        for(Map.Entry<Integer,UserAccount> account:entrySet){
            if(account.getKey().equals(id))
                return account.getValue();
        }
        return null;
    }
//    public static void main(String[] args)throws Exception{
//        DummyDatabase db = new DummyDatabase();
//        UserAccount ac =  new UserAccount();
//        ac.setName("Ana");
//        ac.setEmail("lal@yahoo.com");
//        ac.setPasword("parola");
//        ac.setLogedIn(false);
//        db.addAccount(1, ac);
//        
//         UserAccount ac2 =  new UserAccount();
//        ac2.setName("George");
//        ac2.setEmail("gg@yahoo.com");
//        ac2.setPasword("george");
//        ac2.setLogedIn(false);
//        db.addAccount(2, ac2);
//        db.save();
//    }
}
