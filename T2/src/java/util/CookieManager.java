/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package util;

import java.util.Random;
import javax.servlet.http.Cookie;

/**
 *
 * @author Corina
 */
public class CookieManager {

    public static Cookie deleteCookie(Cookie[] cookies, String cookieName) {
        for (int i = 0; i < cookies.length; i++) {
            Cookie c = cookies[i];
            if (c.getName().equals(cookieName)) {
                c.setMaxAge(0);
                c.setValue(null);
                return c;
            }
        }
        return null;
    }

    public static String generateCookieToken() {
        Random r = new Random();
        int randomInt = r.nextInt() * 1000 + 7;
        String cookieToken = "cookieToken" + randomInt;
        return cookieToken;
    }
    public static Cookie createCookie(String cookieName, String value, int lifetime)
    {
        Cookie c = new Cookie(cookieName,value);
        c.setMaxAge(lifetime);
        return c;
    }
    public static void updateCookie(Cookie c,String value){
        c.setValue(value);
    }
}
