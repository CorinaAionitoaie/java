/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package beans;
import java.io.Serializable;
/**
 *
 * @author Corina
 */
public class UserAccount implements Serializable {
    
    private String name;
    private String email;
    private String password;
    private boolean logedIn;
    
    public void setName(String name){
        this.name=name;
    }
    public String getName(){
        return name;
    }
    public void setEmail(String email){
        this.email=email;
    }
    public String getEmail(){
        return email;
    }
    public void setPasword(String password){
        this.password = password;
    }
    public String getPassword(){
        return password;
    }
    public void setLogedIn(boolean logedIn){
        this.logedIn = logedIn;
    }
    public boolean getLogedIn(){
        return logedIn;
    }
}
