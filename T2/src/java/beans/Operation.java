/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package beans;

import java.math.BigInteger;
import java.io.Serializable;

/**
 *
 * @author Corina
 */
public class Operation implements Serializable {

    private String firstNumber;
    private String secondNumber;
    private String operator;
    private String result;

    public void setFirstNumber(String firstNumber) {
        this.firstNumber = firstNumber;
    }

    public String getFirstNumber() {
        return firstNumber;
    }

    public void setSecondNumber(String secondNumber) {
        this.secondNumber = secondNumber;
    }

    public String getSecondNumber() {
        return secondNumber;
    }

    public void setOperator(String operator) {
        this.operator = operator;
    }

    public String getOperator() {
        return operator;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public String getResult() {
        return result;
    }

    public boolean isValid() {
        if (firstNumber != null && secondNumber != null
                && (operator != null && !operator.equals(""))
                ) {
            return true;
        }
        return false;
    }
}
