/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package servlets;

import beans.UserAccount;
import DummyDatabase.*;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Cookie;
import static constants.Constants.*;
import DummyDatabase.CookiesDatabase;
import beans.Operation;
import java.io.BufferedWriter;
import java.io.FileWriter;
import javax.servlet.http.HttpSession;
import java.util.*;
import javax.servlet.ServletContext;
import util.CookieManager;

/**
 *
 * @author Corina
 */
public class LoginServlet extends HttpServlet {

    private UserAccount user;

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String redirectAddres = "index.jsp";
        Cookie[] cookies = request.getCookies();
        if (cookies != null) {
            for (Cookie c : cookies) {
                if (c.getName().equals(COOKIE_NAME)) {
                    Integer userId = CookiesDatabase.retrieveUserId(c.getValue());
                    try {
                        DummyDatabase db = new DummyDatabase();
                        UserAccount account = db.retrieveAccountById(userId);
//aici                  
                        getUserAccountBean();
                        if (db.checkAccount(account.getName(), account.getPassword())) {
                            user.setLogedIn(true);
                            user.setName(account.getName());
                            redirectAddres = "success.jsp";
                        } else {
                            user.setLogedIn(false);
                            redirectAddres = "index.jsp";
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else {
                    // response.sendRedirect("index.jsp");
                    redirectAddres = "index.jsp";
                }
            }
        } else {
            //response.sendRedirect("login.jsp");
            redirectAddres = "login.jsp";
        }
        response.sendRedirect(redirectAddres);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String name = request.getParameter("name");
        String password = request.getParameter("password");
        String remember = request.getParameter("remember");
        Cookie[] cookies = request.getCookies();
        getUserAccountBean();
        String redirectingAddress = "error.jsp";
        try {
            DummyDatabase db = new DummyDatabase();
            if (db.checkAccount(name, password)) {
                if (remember != null) {
                    addRememberMeBehavior(db, name, cookies, response);
                } else {
                    Cookie c = CookieManager.deleteCookie(cookies, COOKIE_NAME);
                    if (c != null) {
                        response.addCookie(c);
                    }
                }
                user.setLogedIn(true);
                user.setName(name);
                redirectingAddress = "success.jsp";
            } else {
                user.setLogedIn(false);
                request.getSession().setAttribute("error", LOGIN_ERROR);
                redirectingAddress = "error.jsp";
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        response.sendRedirect(redirectingAddress);
    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

    private void addRememberMeBehavior(DummyDatabase db, String userName,
            Cookie[] cookies, HttpServletResponse response) {
        try {
            String cookieToken = CookieManager.generateCookieToken();
            boolean reset = false;
            if (cookies != null) {
                for (int i = 0; i < cookies.length; i++) {
                    if (cookies[i].getName().equals(COOKIE_NAME)) {
                        CookieManager.updateCookie(cookies[i], cookieToken);
                        response.addCookie(cookies[i]);
                        reset = true;
                        break;
                    }
                }
            }
            if (reset == false) {
                Cookie cookie = CookieManager.createCookie(COOKIE_NAME, cookieToken, 365 * 54 * 60 * 60);
                response.addCookie(cookie);
            }
            CookiesDatabase.storeAssociation(cookieToken, db.retrieveAccount(userName).getKey());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private UserAccount getUserAccountBean() {
        ServletContext sc = getServletContext();
        UserAccount userAccount = (UserAccount) sc.getAttribute("userAccountBean");
        if (user == null) {
            user = new UserAccount();
            sc.setAttribute("userAccountBean", user);
        }
        return userAccount;
    }
}
