/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package servlets;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.ServletContext;
import beans.UserAccount;
import DummyDatabase.DummyDatabase;
import java.util.Random;
import javax.servlet.http.HttpSession;
import static constants.Constants.*;

public class RegisterServlet extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String redirectingAddress = "index.jsp";

        boolean captchaOk = checkCaptchaCode(request);
        if (captchaOk == false) {
            addErrorSessionAttribute(request, CAPTCHA_ERROR);
            redirectingAddress = "error.jsp";
        } else {
            ServletContext context = getServletContext();
            UserAccount user = (UserAccount) context.getAttribute("userAccountBean");
            if (user == null) {
                user = new UserAccount();
                context.setAttribute("userAccoutBean", user);
            }
            user.setName(request.getParameter("name"));
            user.setEmail(request.getParameter("email"));
            user.setPasword(request.getParameter("password"));
            user.setLogedIn(false);
            try {
                DummyDatabase db = new DummyDatabase();
                db.addAccount(generateId(), user);
                db.save();
            } catch (Exception ex) {
                ex.printStackTrace();
            }
            redirectingAddress = "login.jsp";
        }
        response.sendRedirect(redirectingAddress);
    }

    private Integer generateId() {
        Random r = new Random();
        int id = r.nextInt() * 100 + 7;
        return new Integer(id);
    }

    private boolean checkCaptchaCode(HttpServletRequest request) {
        HttpSession session = request.getSession();
        String captchaCode = (String) session.getAttribute("captchaCode");
        String userCaptcha = request.getParameter("userCaptcha").trim();
        if (userCaptcha.equals(captchaCode)) {
            return true;
        }
        return false;
    }

    private void addErrorSessionAttribute(HttpServletRequest request, String message) {
        HttpSession session = request.getSession();
        session.setAttribute("error", message);
    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
