/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package servlets;

import java.awt.Color;
import java.awt.Font;
import java.awt.GradientPaint;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.IOException;
import javax.imageio.ImageIO;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Random;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpSession;
import java.io.*;

/**
 *
 * @author Corina
 */
public class ImageGenerator extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("image/png");
        ServletOutputStream out = response.getOutputStream();

        BufferedImage image = new BufferedImage(60, 40, BufferedImage.TYPE_BYTE_INDEXED);

        Graphics2D graphics = image.createGraphics();

        graphics.setColor(new Color(0xccccff));
        graphics.fillRect(0, 0, 60, 40);

        // set gradient font of text to be converted to image
        // GradientPaint gradientPaint = new GradientPaint(10, 5, Color.BLUE, 20, 10, Color.LIGHT_GRAY, true);
        // graphics.setPaint(gradientPaint);
        graphics.setPaint(Color.DARK_GRAY);
        // Font font = new Font("Comic Sans MS", Font.BOLD, 30);
        Font font = new Font("Sans Serif", Font.BOLD, 15);
        graphics.setFont(font);

        // write 'Hello World!' string in the image
        String captchaCode = generateRandomString();
        graphics.drawString(captchaCode, 5, 30);
        addCaptchaCodeToSesion(request, captchaCode);
        // release resources used by graphics context
        graphics.dispose();

        // encode the image as a JPEG data stream and write the same to servlet output stream  
        // JPEGCodec.createJPEGEncoder(out).encode(image);

        // close the stream
        OutputStream os = response.getOutputStream();
        ImageIO.write(image, "png", os);
        os.close();
        out.close();
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

    private String generateRandomString() {
        Random r = new Random();
        StringBuilder randomString = new StringBuilder();
        for (int i = 0; i < 5; i++) {
            randomString.append((char) (r.nextInt(26) + 65));
        }
        System.out.println("Random string " + randomString);
        return randomString.toString();
    }

    private void addCaptchaCodeToSesion(HttpServletRequest request, String captchaCode) {
        HttpSession session = request.getSession();
        session.setAttribute("captchaCode", captchaCode);
    }
}
