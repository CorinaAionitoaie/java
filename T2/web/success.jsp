<%@page contentType="text/html"%>
<!DOCTYPE html>
<html>
    <head>
        <title>Success Page</title>
    </head>
    <body>         
        <p>Hello <jsp:getProperty name="userAccountBean" property="name"/></p>
        <ul>
            <li><a href="compute.jsp"> click to calculate </a></li>
            <li><a href="index.jsp">back to main page</a></li>
        </ul>
</html>
