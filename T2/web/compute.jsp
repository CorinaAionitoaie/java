<%@page contentType="text/html"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<jsp:useBean id="operationBean" class="beans.Operation" scope="request">
<jsp:setProperty name="operationBean" property="*" />
</jsp:useBean>
<c:if test="${operationBean.isValid()}">
    <jsp:forward page="OperationServlet"/>
</c:if>        
<!DOCTYPE html>
<html>
    <head>
        <title>Compute Page</title>
    </head>
    <body>        
        <form id="operationform" method="POST" action="compute.jsp">
            <input type="number" placeholder="first number" name="firstNumber"/><br/>
            <input type="number" placeholder="second number" name="secondNumber"><br/>
        </form>
        <select name="operator" form="operationform">
            <option value="+">+</option>
            <option value="-">-</option>
            <option value="*">*</option>
            <option value="/">/</option>
            <option value="%">%</option>
        </select>
        <br/>
        <input type="submit" value="calculate" form="operationform"/>
    </body>


</html>
