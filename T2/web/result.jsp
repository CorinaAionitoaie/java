<%@page contentType="text/html" %>
<!DOCTYPE html>
<html>
    <head>
        <title>Result Page</title>
    </head>
    <body>
        <p>Result</p>
        <jsp:getProperty name="operationBean" property="firstNumber"/>
        <jsp:getProperty name="operationBean" property="operator"/>
        <jsp:getProperty name="operationBean" property="secondNumber"/>
        = <jsp:getProperty name="operationBean" property="result"/>      
        <br/>
        <ul>
            <li><a href ="compute.jsp"> calculate another operation </a></li>
            <li><a href="index.jsp">login as a different user</a></li>
        </ul>
    </body>
</html>
