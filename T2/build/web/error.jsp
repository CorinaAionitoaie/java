<%@page contentType="text/html" %>
<%@page import="javax.servlet.http.HttpSession,javax.servlet.http.HttpServletRequest" %>
<!DOCTYPE html>
<html>
    <head>
        <title>Error Page</title>
    </head>
    <body>
        <%!String error;%>
        <%
            error = (String) session.getAttribute("error");
        %>
        <%= error%>
        <br/>
        <a href="index.jsp"> back to main page </a>
    </p>
</body>
</html>
